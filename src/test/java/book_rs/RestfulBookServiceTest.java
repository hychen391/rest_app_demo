package book_rs;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.impl.base.exporter.zip.ZipExporterImpl;
import org.junit.Test;
import web_container_util.SimpleEmbeddedRunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;

public class RestfulBookServiceTest {

    @Test
    public void shouldAssembleAndRetrieveBookList() throws Exception{
        WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .addClass(RestfulBookService.class)
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        // the target war file
        File warFile = new File(webArchive.getName());
        //
        (new ZipExporterImpl(webArchive)).exportTo(warFile, true);

        // Invoke the embedded runner
        SimpleEmbeddedRunner runner = SimpleEmbeddedRunner.launchDeployWarFile(warFile, "rest_app", 8082);

        try{
            URL url = new URL("http://localhost:8082/rest_app/rest/books");
            InputStream is = url.openStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            List<String> lines = new ArrayList<>();
            String text = null;
            int count =0;
            while ((text = reader.readLine())!= null){
                lines.add(text);
                count++;
                System.out.printf("*** Output *** text[%d] = %s \n", count, text);
                assertFalse(lines.isEmpty());
            }

        }
        finally {
            runner.stop();
        }

    }
}