package entities;

import org.junit.Test;

import static org.junit.Assert.*;

public class DishTest {

    @Test
    public void toJSON() {
        Dish dish = new Dish(1, "fish", 30);
        String actual = dish.toJSON();
        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append("\"id\":1").append(",")
                .append("\"name\":\"fish\"").append(",")
                .append("\"price\":30.0")
                .append("}");
        assertEquals(sb.toString(), actual);
    }
}