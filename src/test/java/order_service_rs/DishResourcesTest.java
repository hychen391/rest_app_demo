package order_service_rs;

import book_rs.RestfulBookService;
import entities.Dish;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.impl.base.exporter.zip.ZipExporterImpl;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import webUtil.WebMethodUtils;
import web_container_util.SimpleEmbeddedRunner;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.*;

public class DishResourcesTest {
    static final String WEB_CONTEXT = "rest_app";
    static final String PORT = "8082";
    static final String TARGET_SERVICE_URI = "http://localhost:8080/rest_app2-1.0-SNAPSHOT/os_rest/dishes/";
    static SimpleEmbeddedRunner embeddedRunner;

    @BeforeClass
    public static void beforeAllTests(){
        // Deploy the war file before running the test on the embedded glassfish.
//        try {
//            deployWar();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @AfterClass
    public static void afterAllTests() throws Exception {
//        embeddedRunner.stop();
    }



    @Test
    public void getUser() throws Exception {
        // Fixme: https://bitbucket.org/hychen391/rest_app_demo/issues/1/fail-to-inject-single-bean-to
        // The url for testing in the embedded glassfish
//        String urlStr = "http://localhost:" + PORT + "/" + WEB_CONTEXT +"/os_rest/dishes/1";
        // The url for testing in the local glassfish server.
        String urlStr = "http://localhost:8080/rest_app2-1.0-SNAPSHOT/os_rest/dishes/2";
        //make the get request
        URL tgtUrl = new URL(urlStr);
        List<String> responses = WebMethodUtils.makeGetRequest(tgtUrl);
        assertNotEquals(responses.size(), 0);

    }

    /**
     * The web context is {@code rest_app}
     * @throws Exception
     */
    private static void deployWar() throws Exception {
        WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .addClass(RestfulBookService.class)
                .addClass(DishResources.class)
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml"); // to enable CDI
        // the target war file
        File warFile = new File(webArchive.getName());
        //
        (new ZipExporterImpl(webArchive)).exportTo(warFile, true);

        // Invoke the embedded runner
        embeddedRunner = SimpleEmbeddedRunner.launchDeployWarFile(warFile, "rest_app", 8082);

    }

    /**
     * Test {@link DishResources#addDish(String)}
     *
     * Issue: https://bitbucket.org/hychen391/rest_app_demo/issues/2/create-a-dish-record-using-post-request
     * DoneToDo: Run the test; make it pass.
     */
    @Test
    public void addDish() throws Exception {
        String tgtUrlstr = "http://localhost:8080/rest_app2-1.0-SNAPSHOT/os_rest/dishes/add";

        //Create a Dish object
        Dish item = new Dish(999, "Spicy Chicken 宮堡雞丁", 150);
        String itemJsonStr = item.toJSON();
        System.out.printf("**Data to be posted** %s \n", itemJsonStr);

        // Test the conversion
        Dish expectedDish = DishResources.parseJsonObjectToDish(itemJsonStr);
        assertNotNull(expectedDish);

        // Make a post request
        /*
          FixedFixme: javax.json.stream.JsonParsingException: Unexpected char 1 at (line no=1, column no=36, offset=35)
          at order_service_rs.DishResources.parseJsonObjectToDish(DishResources.java:82)
          See: https://stackoverflow.com/a/18760096/7820390
        */
        URL tgtUrl = new URL(tgtUrlstr);
        List<String> responses = WebMethodUtils.makeWebRequest("POST", tgtUrl, MediaType.APPLICATION_JSON, itemJsonStr);
        System.out.println(responses.toString());
        assertNotEquals(responses.size(), 0);

    }

    /**
     *
     * ToDo: Open
     */
    @Test
    public void updateDish() {
    }

    /**
     * Test case: remove the Dish Item whose id is 2.
     *
     */
    @Test
    public void removeDish() throws Exception {
        String sizUrlStr = TARGET_SERVICE_URI + "size";
        String tgtUrlStr = "http://localhost:8080/rest_app2-1.0-SNAPSHOT/os_rest/dishes/remove/2";

        URL sizeUrl = new URL(sizUrlStr);
        List<String> responses = WebMethodUtils.makeGetRequest(sizeUrl);
        int oldSize = Integer.valueOf(responses.get(0)).intValue();

        URL tgtUrl = new URL(tgtUrlStr);
        responses = WebMethodUtils.makeDeleteRequest(tgtUrl);

        int newSize =  Integer.valueOf(responses.get(0)).intValue();
        assertNotEquals(oldSize, newSize);
    }
}