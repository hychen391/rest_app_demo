package book_rs;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Arrays;
import java.util.List;

@Path("/books")
public class RestfulBookService {

    private List<Book> products = Arrays.asList(
            new Book("Author1", "Title 1"),
            new Book("Author2", "Title 2"),
            new Book("Author3", "Title 3")
    );

    @GET @Produces("text/plain")
    public String getList(){
        StringBuffer sb = new StringBuffer();
        products.stream().forEach((book)->{
            sb.append(book.title).append('\n');
        });

        return sb.toString();
    }

    @PostConstruct
    public void acquireResource(){

    }

    @PreDestroy
    public void releaseResource(){

    }

    static class Book {
        public final String author;
        public final String title;

        Book(String author, String title) {
            this.author = author;
            this.title = title;
        }
    }


}
