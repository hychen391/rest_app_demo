package order_service_rs;

import entities.Dish;
import order_service_rs.dataResp.DishRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.PostActivate;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringReader;

/**
 * Dish RESTful resource. (EBJ Component)
 * <p>
 * url: /project_context/os_rest/dishes
 */
@Path("/dishes")
@Stateless
@LocalBean
public class DishResources {
    //Inject the Dish Repository
    @EJB(name = "dishRepositoryEJB")
    DishRepository dishRepository = new DishRepository();

    /**
     * Return a specified dish by ID.
     * <p>
     * GET request handler; Media type: application/json
     * URL: /project_context/or_rest/dishes/{id}
     * <p>
     * Use {@code @PathParam} to inject the path parameter {id} to the method.
     *
     * Fixme: should return message when cannot find the Dish using the ID
     */
    @GET
    @Path("{id}")
    @Produces("application/json")
    public String getDish(@PathParam("id") long id) {
        Dish dish = dishRepository.findById(id);
        return dish.toJSON();
    }

    /**
     * Add a Dish to the reposity.
     * <p>
     * POST request handler; Media type: applicatin/json <br/>
     * URL:/project_context/or_rest/dishes/add
     * <p>
     * Tech Ref:
     *
     * @return number of the Dish objects in the repository
     * @See Dish
     */
    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    public String addDish(String data) {
        // create the json object from the data
        // convert String to string stream
        System.out.printf("**Post Data** %s", data);
        Dish dish = parseJsonObjectToDish(data);
        // add to the repository
        System.out.printf("**Created Dish instance** %s", dish.toJSON());
        dishRepository.add(dish);
        return String.valueOf(dishRepository.size());
    }

    /**
     * Update operation for the Dish Repository.
     * @param data
     * @return
     *
     * ToDo: Testing
     */
    @POST
    @Path("update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDish(String data){
        Response response = null;
        System.out.printf("**Post Data** %s", data);
        Dish dish = parseJsonObjectToDish(data);
        // update to the repository
        System.out.printf("**Update Dish instance** %s", dish.toJSON());
        dishRepository.update(dish);
        response = Response.ok().build();
        return response;
    }

    /**
     * Remove operation for the Dish Repository.
     * @param idStr
     * @return
     *
     */
    @DELETE
    @Path("remove/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response removeDish(@PathParam("id") String idStr){
        long id = Long.valueOf(idStr);
        System.out.printf("**Dish ID to remove: %d", id);
        boolean op_result = dishRepository.remove(id);
        if (op_result)
        return Response.ok()
                .entity(String.valueOf(dishRepository.size()))
                .type(MediaType.TEXT_PLAIN)
                .build();
        else
            return Response.status(Response.Status.NOT_MODIFIED)
                    .entity("remove operation fails.")
                    .build();
    }

    @GET
    @Path("size")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getSize(){

        return Response.ok()
                .entity(String.valueOf(dishRepository.size()))
                .type(MediaType.TEXT_PLAIN)
                .build();
    }
    /**
     * Parse JsonObject instance to the Dish instance
     *
     * @return
     */
    public static Dish parseJsonObjectToDish(String jsonObjectString) {
        Dish dish = null;
        StringReader stringReader = new StringReader(jsonObjectString);
        // use javax.json.Json to create a json reader to read data from a string.
        // Pasing JSON with the object model not the streaming
        JsonReader jsonReader = Json.createReader(stringReader);
        JsonObject jsonObject = jsonReader.readObject();
        try {
            long itemId = Long.valueOf(jsonObject.getInt("id"));
            double price = jsonObject.getJsonNumber("price").doubleValue();

            dish = new Dish(itemId,
                    jsonObject.getString("name"),
                    price);
        } catch (Exception e) {
            System.out.printf("**Exception** Fail to convert the JsonObject to Dish");
            e.printStackTrace();
        } finally {

            return dish;
        }

    }
}
