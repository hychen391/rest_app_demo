package order_service_rs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("os_rest")
public class OrderServiceRestApp extends Application {

}
