package order_service_rs.dataResp;

import entities.Dish;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Dish Repository
 */
//@Singleton(name = "dishRepositoryEJB")
@Stateless(name = "dishRepositoryEJB")
// Need add the annotation. Otherwise, the container cannot find the ejb name and inject it.
// Cause the exception:
// javax.naming.NameNotFoundException: order_service_rs.dataResp.DishRepository#order_service_rs.dataResp.DishRepository not found
@LocalBean
@Startup  // load the EJS at the startup
public class DishRepository implements RepositoryFacade<Dish>{
    private Map<Long, Dish> repository = new ConcurrentHashMap<>();

    @Override
    public boolean add(Dish item) {
        repository.put(item.getId(), item);
        return true;
    }

    @Override
    public boolean remove(long itemId) {
        if (repository.containsKey(itemId)){
            repository.remove(itemId);
            return true;
        } else
            return false;
    }

    @Override
    public Dish findById(long id) {
        if (repository.containsKey(id))
            return repository.get(id);
        else
            return null;
    }

    @Override
    public List<Dish> findAll() {
        List<Dish> allItems = new ArrayList<>(repository.values());
        return allItems;
    }

    @Override
    public int size() {
        return repository.size();
    }

    @PostConstruct
    public void init(){
        this.add(new Dish(1, "雞腿飯", 85));
        this.add(new Dish(2, "排骨飯", 80));
    }

    @Override
    public boolean update(Dish item) {
        boolean op_result = false;
        if (repository.containsKey(item.getId())){
            repository.put(item.getId(), item);
            op_result = true;
        }
        return op_result;
    }
}
