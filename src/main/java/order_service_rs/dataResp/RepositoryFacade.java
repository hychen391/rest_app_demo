package order_service_rs.dataResp;

import java.util.List;

/**
 * Operations for the repository.
 * @param <T> Class of the object.
 */
public interface RepositoryFacade<T> {
     boolean add(T item);
     boolean remove(long id);
     T findById(long id);
     List<T> findAll();

     /**
      * Update item
      * @param item
      * @return true if update successfully.
      */
     boolean update(T item);

     /**
      * Size of the repository.
      * @return
      */
     int size();
}

