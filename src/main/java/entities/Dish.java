package entities;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import java.io.Serializable;
import java.io.StringWriter;

/**
 * 餐點
 */
public class Dish implements Serializable {
    // fields
    private long id;
    private String name;
    private double price;

    public Dish(long id, String dishName, double price) {
        this.id = id;
        this.name = dishName;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    /**
     * Return the JSON representation of the object.
     * @return
     */
    public String toJSON(){
        StringWriter sw = new StringWriter();
        // Generate JSON with Streaming API: JSON-P
        // Advantage: Save the amount of memory for the large json document.
        // The json output saved in the StringWriter
        try (JsonGenerator generator = Json.createGenerator(sw)){
            generator
                    .writeStartObject()
                        .write("id", this.getId())
                        .write("name", this.getName())
                        .write("price", this.getPrice())
                    .writeEnd();
        }

        return sw.toString();

    }

    @Override
    public int hashCode() {
        return new Long(id).intValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Dish){
            return (this.id == ((Dish) obj).id);
        } else
            return false;
    }
}
