/*******************************************************************************
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013,2014 by Peter Pilgrim, Addiscombe, Surrey, XeNoNiQUe UK
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU GPL v3.0
 * which accompanies this distribution, and is available at:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * Developers:
 * Peter Pilgrim -- design, development and implementation
 *               -- Blog: http://www.xenonique.co.uk/blog/
 *               -- Twitter: @peter_pilgrim
 *
 * Contributors:
 *
 *******************************************************************************/

package webUtil;

import javax.ws.rs.core.MediaType;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The type WebMethodUtils
 *
 * @author Peter Pilgrim (peter)
 * @author hychen39@gmailcom (contributor)
 */
public class WebMethodUtils {
    private WebMethodUtils() {}

    /**
     * Use to get lines from the response.
     * @param inputStream
     * @return
     * @throws Exception
     */
    public static List<String> getLines( InputStream inputStream) throws Exception {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(inputStream));
        List<String> lines = new ArrayList<>();
        String text = null;
        int count=0;
        while ( ( text = reader.readLine()) != null ) {
            lines.add(text);
            System.out.printf("**** OUTPUT **** text[%d] = %s\n", count, text);
            ++count;
        }
        return lines;
    }

    /**
     * Make GET request.
     * @param url
     * @return Content of the response
     * @throws Exception
     */
    public static List<String> makeGetRequest( URL url ) throws Exception {
        // the default is GET request
        // openStream = openConnection().getInputStream()
        InputStream inputStream = url.openStream();
        return getLines(inputStream);
    }

    /**
     * Make DELETE request
     * @param url
     * @return Content of the response
     * @throws Exception
     */
    public static List<String> makeDeleteRequest( URL url ) throws Exception {

        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestProperty(
                "Content-Type", "application/x-www-form-urlencoded" );
        httpCon.setRequestMethod("DELETE");
        httpCon.connect();

        InputStream inputStream = httpCon.getInputStream();
        return getLines(inputStream);
    }

    public static List<String> makePostRequest( URL url, Map<String, String> params)
            throws Exception {
        return makeWebRequest("POST", url, params);
    }

    public static List<String> makePutRequest( URL url, Map<String, String> params)
            throws Exception {
        return makeWebRequest("PUT", url, params);
    }

    /**
     * Make web request
     * @param requestMethod
     * @param url
     * @param params
     * @return response content
     * @throws Exception
     */
    public static List<String> makeWebRequest( String requestMethod,
                                               URL url, Map<String,String> params)
            throws Exception {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setUseCaches(false);
        connection.setRequestMethod(requestMethod);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("charset", "utf-8");

        StringBuilder urlParameters = new StringBuilder();
        for ( Map.Entry<String,String> entry: params.entrySet()) {
            urlParameters.append(entry.getKey()+
                    "="+entry.getValue()+"&");
        }

        // write the parameters
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
        wr.writeBytes(urlParameters.toString());
        wr.flush();
        wr.close();
        connection.disconnect();

        InputStream inputStream = connection.getInputStream();
        return getLines(inputStream);
    }

    /**
     * Make the web request by the giving parameters: request method, url, media type, and payload content.
     * @param requestMethod
     * @param url
     * @param mediaType
     * @param payloadContent
     * @return
     * @throws Exception
     */
    public static List<String> makeWebRequest (String requestMethod,
                                               URL url,
                                               String mediaType,
                                               String payloadContent) throws Exception{
        // Create connection
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setUseCaches(false);
        connection.setRequestMethod(requestMethod);
        connection.setRequestProperty("Content-Type", mediaType);
        connection.setRequestProperty("charset", "utf-8");

        // wirte the payload content
        // To write utf-8 characters, see: https://stackoverflow.com/a/18760096/7820390
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(wr, "utf-8"));
//        wr.writeBytes(payloadContent);
        bw.write(payloadContent);
        bw.flush();
        bw.close();
        wr.close();
        connection.disconnect();

        // get the response
        InputStream inputStream = connection.getInputStream();
        return getLines(inputStream);

    }
}